# Description:
#   Deal with it, Targaryen style.
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   When hubot hears "deal with it".
#
# Author:
#   antonelli

module.exports = (robot) ->
  robot.hear /deal with it/i, (message) ->
    message.send "http://i.imgur.com/3tHNWH1.gif"
