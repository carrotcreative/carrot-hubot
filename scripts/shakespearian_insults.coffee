# Description:
#   Because sometimes curses are not enough.
#
# Commands:
#   hubot release the shakespearian insults
#

request = require 'request'

module.exports = (robot) ->

  robot.respond /release the shakespearian insults/i, (msg) ->
    request uri: 'http://www.pangloss.com/seidel/Shaker/index.html', (err, res, body) ->
      msg.send body.match(/<font size="\+2">\s(.*)<\/font>/)[1].replace(/<br>/, " ")

  robot.hear /insult/i, (msg) ->
    request uri: 'http://www.pangloss.com/seidel/Shaker/index.html', (err, res, body) ->
      msg.send body.match(/<font size="\+2">\s(.*)<\/font>/)[1].replace(/<br>/, " ")