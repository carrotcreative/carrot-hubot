# Description:
#   Hubot is a gracious bot
#
# Commands:
#   thanks hubot or thank you hubot

module.exports = (robot) ->
  robot.hear /(thanks hubot|thank you hubot)/i, (msg) ->
    msg.send "you're most welcome!"
