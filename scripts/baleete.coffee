# Description:
#   Because sometimes things need to be baleeted
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   whenever you say deleted or baleeted
#
# Author:
#   jenius

module.exports = (robot) ->
  robot.hear /\s?(deleted?|baleeted?)\s?/i, (message) ->
    message.send "http://cl.ly/GVPS/baleete.gif"
