# Description:
#   Very very sneaky
#
# Commands:
#   "sneak peak"

module.exports = (robot) ->
  robot.hear /\s?(sneak peak)\s?/i, (msg) ->
    msg.send 'I think you mean "sneak peek"'
