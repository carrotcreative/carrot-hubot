# Description:
#   Staying in shape son.
#
# Commands:
#   hubot track push ups
#   hubot stop tracking
#   hubot are you tracking?
#   hubot jeff did 20 push ups
#   hubot how many push ups has jeff done?

cronJob = require('cron').CronJob;
push_up_images = ["http://cl.ly/Giz8/08StandardPushup.gif", "http://cl.ly/GjpN/1332356826_worlds_strongest_kid_giuliano_sroe_doing_pushups_on_bottles.gif", "http://cl.ly/GiiO/tumblr_m1qbvnxphU1qitnnao1_500.gif", "http://cl.ly/GjTF/tumblr_m2bxnuHW5K1qhvl0bo1_1280.png", "http://cl.ly/GjZx/RichardSimmons_t607.jpeg", "http://cl.ly/Gj2V/27331478.jpeg", "http://cl.ly/MEHK/peetamellark8.tumblr.jpeg", "http://cl.ly/MDp5/tumblr_m3f110ZclS1r2861wo1_500.jpeg", "http://cl.ly/MEO6/tumblr_m46e8ruswC1qj3wigo1_500.gif"]
tracker = {}

module.exports = (robot) ->

  job = null

  robot.respond /track push ups/i, (msg) ->
    msg.send "Ok - I'll remind you every hour"

    # (cron format: sec, min, hour, day, month, day of week)
    # job = new cronJob '00 00 09-18 * * 1-5', ->
    job = new cronJob '00 00 01-24 * * 1-5', ->

      current_hour = new Date().getHours()
      hour_formatted = if current_hour > 12 then current_hour -= 12 else current_hour

      console.log "date: #{new Date()}"
      console.log "hour: #{current_hour}"
      console.log "hour formatted: #{hour_formatted}"
      console.log '----------------'

      msg.send "It's #{hour_formatted}:00 - push up time!"
      # msg.send push_up_images[Math.floor(Math.random()*push_up_images.length)]

    , null, true, "America/New_York"

    robot.respond /stop tracking/i, (msg) ->
      msg.send "No longer tracking push ups"
      job.stop()

  robot.respond /.*tracking\?/i, (msg) ->
    if job
      msg.send "Yes I am! Stop it by telling me 'stop tracking'"
    else
      msg.send "Nope, but I can be if you tell me to 'track push ups'"

  robot.respond /(\w+) did (\d+) push ups/i, (msg) ->
    name = msg.match[1]
    number_of_push_ups = parseInt(msg.match[2])

    if number_of_push_ups > 100
      msg.send "nice try #{name}, i know you're not that jacked"
      msg.send "http://24.media.tumblr.com/tumblr_lpspft8tzJ1qchccko1_500.gif"
    else
      tracker[name] = 0 if !tracker[name]
      tracker[name] += number_of_push_ups
      msg.send "Nice work #{name}, I logged all #{number_of_push_ups}"

  robot.respond /how many push ups has (\w+) done/i, (msg) ->
    name = msg.match[1]
    if !tracker[name]
      msg.send "#{name} hasn't done any push ups. what a lazy bum."
    else
      msg.send "#{name} has done #{tracker[name]} push ups"
    

